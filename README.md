# MSMC-CLI - Minecraft Server Management Console CLI
![](https://img.shields.io/badge/version-0.0.2-blue)
![](https://img.shields.io/badge/license-AGPLv3-blue)
```
 __  __  _____ __  __  _____       _____ _      _____ 
|  \/  |/ ____|  \/  |/ ____|     / ____| |    |_   _|
| \  / | (___ | \  / | |   ______| |    | |      | |  
| |\/| |\___ \| |\/| | |  |______| |    | |      | |  
| |  | |____) | |  | | |____     | |____| |____ _| |_ 
|_|  |_|_____/|_|  |_|\_____|     \_____|______|_____|
                                                      
                                                      
MSMC-CLI v0.0.2
Copyright (C) 2023 Noveria Network

This program may be freely redistributed under the terms of the GNU AGPLv3
```

A shell script, which manages minecraft server instances using podman.
Download and setup server instances with ease and scale using only one command (with alot of parameters :P)

Default shell is `/bin/zsh`, because I can.

## Usage
```
usage: msmc-cli [operation]
operations:
        msmc-cli {-h --help}
        msmc-cli {-V --version}
        msmc-cli {-v --verbose}
        msmc-cli {-n --dry-run}
        msmc-cli {-r --runner} [serverrunner e.g. spigot]
        msmc-cli {-m --minecraft-version} [minecraft-version]
        msmc-cli {-p --proxy} [proxyrunner e.g. waterfall]
```