# Changelog
## v0.0.2
---

### Additions
* Add `CHANGELOG.md`
* Add `AGPLv3` License
* Add `README.md`

### Edits
* Edit `usage()` function

### Removals
* Remove `-b, --build` parameter