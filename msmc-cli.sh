#!/bin/zsh

#
# CONSTANTS
#
PKGVER="0.0.2"
PKGNAME="MSMC-CLI"
LICENSE="GNU AGPLv3"
HELPERFILESPATH="./helper"
declare -A SERVERRUNNERS
SERVERRUNNERS["paper"]="https://api.papermc.io/v2/projects/paper/versions"
SERVERRUNNERS["multipaper"]="https://multipaper.io/api/v2/projects/multipaper/versions"
declare -A PROXYRUNNERS
PROXYRUNNERS["waterfall"]="https://api.papermc.io/v2/projects/waterfall/versions"
PROXYRUNNERS["multipaper"]="https://multipaper.io/api/v2/projects/multipaper/versions"

#
# VARIABLES
#
declare dryrun=false
declare paperservercount=0
declare installwaterfall=false
declare serverrunner
declare serverbuild
declare proxyrunner
declare proxybuild
declare version

function msmc-cli() {
    print -P "usage: $0 [operation]\noperations:\n\t$0 {-h --help}\n\t$0 {-V --version}\n\t$0 {-v --verbose}\n\t$0 {-n --dry-run}\n\t$0 {-r --runner} [serverrunner e.g. spigot]\n\t$0 {-m --minecraft-version} [minecraft-version]\n\t$0 {-p --proxy} [proxyrunner e.g. waterfall]"
}

function version() {
    figlet -f big "$PKGNAME"
    print -P "$PKGNAME v$PKGVER\nCopyright (C) 2023 Noveria Network\n\nThis program may be freely redistributed under the terms of the $LICENSE"
}

function checkRunners() {
    if [[ -z "$SERVERRUNNERS[\"$serverrunner\"]" ]]; then
        log e "Serverunner '$serverrunner' is not yet available with this cli-tool."
        log e "If you think, this might be an error, please contact the maintainer of this program, by opening an issue-ticket!"
        log e "GitLab-Repo: https://gitlab.com/noveria/tools/msmc-cli/-/issues"
        log e "Aborting..."
        exit 1
    fi
    if [[ ! -z "$proxyrunner" ]]; then
        if [[ -z "$PROXYRUNNERS[\"$proxyrunner\"]" ]]; then
            log e "Proxyrunner '$proxyrunner' is not yet available with this cli-tool."
            log e "If you think, this might be an error, please contact the maintainer of this program, by opening an issue-ticket!"
            log e "GitLab-Repo: https://gitlab.com/noveria/tools/msmc-cli/-/issues"
            log e "Aborting..."
            exit 1
        fi
    fi
}

function checkVersionExistance() {
    if [[ $(curl -s $SERVERRUNNERS["$serverrunner"]/$version | jq -r '.error') != "null" ]]; then
        log e "Runner '$serverrunner' does not have a '$version' release available yet."
        log e "Aborting..."
        exit 1
    fi
    if [[ ! -z "$proxyrunner" ]]; then
        if [[ "$proxyrunner" == "multipaper" ]]; then
            if [[ $(curl -s $PROXYRUNNERS["$proxyrunner"]/$version | jq '.error') != "null" ]]; then
                log e "Proxyrunner '$proxyrunner' does not have a '$version' release available yet."
                log e "Aborting..."
                exit 1
            fi
        else
            if [[ $(curl -s $PROXYRUNNERS["$proxyrunner"]/$(echo $version | grep . | cut -c-4) | jq '.error') != "null" ]]; then
                log e "Proxyrunner '$proxyrunner' does not have a '$(echo $version | grep . | cut -c-4)' release available yet."
                log e "Aborting..."
                exit 1
            fi
        fi
    fi
}

function setBuildNumbers() {
    serverbuild=$(curl -s "$SERVERRUNNERS[\"$serverrunner\"]/$version" | jq -r ".builds[(($(curl -s "$SERVERRUNNERS[\"$serverrunner\"]/$version" | jq -r ".builds[]" | wc -l)-1))]")
    if [[ $proxyrunner == "multipaper" ]]; then
        proxybuild=$(curl -s "$PROXYRUNNERS[\"$proxyrunner\"]/$version" | jq -r ".builds[(($(curl -s "$PROXYRUNNERS[\"$proxyrunner\"]/$version" | jq -r ".builds[]" | wc -l)-1))]")
    else
        proxybuild=$(curl -s "$PROXYRUNNERS[\"$proxyrunner\"]/$(echo $version | grep . | cut -c-4)" | jq -r ".builds[(($(curl -s "$PROXYRUNNERS[\"$proxyrunner\"]/$(echo $version | grep . | cut -c-4)" | jq -r ".builds[]" | wc -l)-1))]")
    fi
}

source $HELPERFILESPATH/scriptutil.sh

TEMP=$(getopt -o hVvnm:r:p: --long help,version,verbose,dry-run,minecraft-version:,runner:,proxy: -n 'msmc-cli' -- "$@")
if [ "$?" != 0 ] ; then log e "Aborting..." >&2 ; exit 1 ; fi

eval set -- "$TEMP"
while true; do
    case "$1" in
        -h | --help) msmc-cli; exit 0 ;;
        -V | --version) version; exit 0 ;;
        -v | --verbose) debuglevel=4 ;;
        -n | --dry-run) dryrun=true;;
        -r | --runner) shift; serverrunner=$1;;
        -m | --minecraft-version) shift; version=$1;;
        -p | --proxy) shift; proxyrunner=$1;;
        -- ) shift; break;;
        * ) break ;;
    esac
    shift
done

#
# IMPORTS
#
for file in $HELPERFILESPATH/*; do
    if [[ "$(basename $file)" != "scriptutil.sh" ]]; then
        log i "Importing $(basename $file)"
        source "$file"
    fi
done

#
# EXECUTE IN ORDER
#
checkRunners
checkVersionExistance
setBuildNumbers
log i "$proxyrunner: $proxybuild"
log i "$serverrunner: $serverbuild"